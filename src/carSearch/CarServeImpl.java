package carSearch;

import java.util.ArrayList;
import java.util.List;

public class CarServeImpl implements CarService {

	@Override
	public List<Car> findAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Car> search(String keyword) {
		List<Car> tem = new ArrayList<Car>();
		for(Car car : allCars()){
			if(car.getMake().equalsIgnoreCase(keyword)){
				tem.add(car);
			}
		}
		return tem;
	}
	public List<Car> allCars(){
		List<Car> cars = new ArrayList<Car>();
		
		Car car = new Car();
		car.setMake("Honda");
		car.setModel("2013");
		car.setPrice(1000);
		car.setDecription("Make in US");
		car.setPriview("Preview");
		cars.add(car);
		
		car = new Car();
		car.setMake("Hynda");
		car.setModel("2013");
		car.setPrice(10000);
		car.setDecription("Make in Japan");
		car.setPriview("Preview");
		cars.add(car);
		return cars;
		
	}

}
