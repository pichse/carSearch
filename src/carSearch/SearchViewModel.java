package carSearch;
import java.util.List;

import org.zkoss.bind.annotation.*;


public class SearchViewModel {
	
	    private String keyword;
	    private List<Car> carList;
	    private Car selectedCar;
	     
	    
	 
	    public String getKeyword() {
			return keyword;
		}



		public void setKeyword(String keyword) {
			this.keyword = keyword;
		}



		public List<Car> getCarList() {
			if(carList == null){
				carList =  new CarServeImpl().allCars();
				
			}
			return carList;
			
		
		}



		public void setCarList(List<Car> carList) {
			this.carList = carList;
		}



		public Car getSelectedCar() {
			return selectedCar;
		}



		public void setSelectedCar(Car selectedCar) {
			this.selectedCar = selectedCar;
		}


		private CarServeImpl carService = new CarServeImpl();
		@Command
		@NotifyChange("carList")
		public void search(){
	        carList = carService.search(keyword);
		}
}
